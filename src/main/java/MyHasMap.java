import java.util.ArrayList;
import java.util.List;

public class MyHasMap<K, V> implements MyMap<K, V> {
    int size = 0; //насколько заполнен buckets
    int capacity = 16; //насколько заполнен buckets
    Bucket<K, V>[] buckets = new Bucket[capacity];

    @Override
    public int size() {
        return size;
    }

    @Override
    public void put(K key, V value) {
        resize();
        int bucketIndex = getBucketIndex(key);
        Bucket<K, V> bucket = buckets[bucketIndex];
        if (bucket == null) { // Бакет с таким key отсутсвует
            buckets[bucketIndex] = new Bucket<>(key, value);
            size++;
        } else { // collision Бакет с таким key уже есть
            Pair<K, V> pair = findPairWithEqualsKey(bucket, key);
            if (pair == null) { // добавляем еще одну колизию
                bucket.entries.add(new Pair<>(key, value));
                size++;
            } else { //это не колизия. Заменяем.
                pair.value = value;
            }
        }
    }

    @Override
    public V get(K key) {
        int bucketIndex = getBucketIndex(key);
        Pair<K, V> pair = findPairWithEqualsKey(buckets[bucketIndex], key);
        return pair == null ? null : pair.value;
    }

    @Override
    public boolean contains(K key) {
        return get(key) != null;
    }

    private Pair<K, V> findPairWithEqualsKey(Bucket<K, V> bucket, K key) {
        for (Pair<K, V> pair : bucket.entries) {
            if (pair.key.equals(key)) {
                return pair;
            }
        }
        return null;
    }

    private void resize() {
        if (size == capacity * 0.75) {
            Bucket<K, V>[] oldBuckets = buckets;
            capacity = capacity * 2;
            buckets = new Bucket[capacity];
            size = 0;
            for (Bucket<K, V> oldBucket : oldBuckets) {
                if (oldBucket != null) {
                    for (Pair<K, V> pair : oldBucket.entries) {
                        put(pair.key, pair.value);
                    }
                }
            }
        }
    }

    private int getBucketIndex(K key) {
        return Math.abs(key.hashCode()) % buckets.length;
    }

    static class Pair<K, V> { // Пара значений
        K key;
        V value;

        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    static class Bucket<K, V> { // Масив пар значений. Содержит одно значение если нет колизий и больше одно если есть колизии
        List<Pair<K, V>> entries = new ArrayList<>();

        public Bucket(K key, V value) {
            this.entries.add(new Pair<>(key, value));
        }
    }
}
