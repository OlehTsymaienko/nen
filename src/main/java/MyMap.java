public interface MyMap<K, V> {
    int size();
    void put(K key, V value);
    V get(K key);
    boolean contains(K key);
}