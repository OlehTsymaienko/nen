public class MyArrayList<TYPE> implements MyList<TYPE> {
    int size = 0;
    TYPE[] array = (TYPE[]) new Object[2];

    public int size() { // O(1)
        return size;
    }

    public void add(TYPE i) { // O(1)
        checkSize();
        array[size] = i;
        size++;
    }

    private void checkSize() {
        if (array.length <= size) {
            TYPE[] tmp = (TYPE[]) new Object[size * 2];
            for (int i = 0; i < array.length; i++) {
                tmp[i] = array[i];
            }
            array = tmp;
        }
    }

    public TYPE get(int i) {// O(1)
        return array[i];
    }

    public void delete(int index) { // O(1)
        for (int i = index; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        size--;
    }

    @Override
    public boolean contains(TYPE i) { // O(n)  //O(1)  //O(log(n)) ; O(n^2)
        for (TYPE var : array) {
            if (var == i) {
                return true;
            }
        }
        return false;
    }


}
