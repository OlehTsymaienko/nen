public interface MyList <TYPE> {
    int size();
    void add(TYPE i);
    TYPE get(int i);
    void delete(int i);
    boolean contains(TYPE i);
}
