public class MyLinkedList<TYPE> implements MyList<TYPE> {
    Node<TYPE> root = new Node<>();
    int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(TYPE i) { // O(n)
        Node<TYPE> current = root;
        while (current.nasNext()) {
            current = current.next;
        }
        current.next = new Node<>();
        current.next.value = i;
        size++;
    }

    @Override
    public TYPE get(int i) { // O(n)
        return getNode(i).value;
    }

    public Node<TYPE> getNode(int i) { // O(n)
        Node<TYPE> current = root;
        int n = 0;
        while (current.nasNext()) {
            current = current.next;
            if (n == i) {
                return current;
            }
            n++;
        }
        throw new RuntimeException("Index out of bound");
    }

    @Override
    public void delete(int i) { //O(n)
        Node<TYPE> prev = getNode(i - 1);  // i-1
        Node<TYPE> current = prev.next;   // i
        Node<TYPE> next = current.next;   // i+1
        prev.next = next;
        size--;
    }

    @Override
    public boolean contains(TYPE i) { //O(n)
        Node<TYPE> current = root;
        while (current.nasNext()) {
            current = current.next;
            if (current.value == i) {
                return true;
            }
        }
        return false;
    }

    static class Node<T> {
        T value;
        Node<T> next;

        public boolean nasNext() {
            return next != null;
        }
    }
}
