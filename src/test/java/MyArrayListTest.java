import static org.junit.Assert.*;

import org.junit.Test;

public class MyArrayListTest {

    @Test
    public void size() {
        MyList<Integer> list = new MyArrayList<Integer>();
        assertEquals(0, list.size());
        list.add(22);
        assertEquals(1, list.size());
        list.add(33);
        assertEquals(2, list.size());
        list.add(44);
        assertEquals(3, list.size());
        list.add(44);
        assertEquals(4, list.size());
    }

    @Test
    public void add() {
        MyList<Integer> list = new MyArrayList<Integer>();
        list.add(22);
        list.add(33);
        list.add(44);
        list.add(55);
        list.delete(4);
        assertEquals(22, list.get(0),0);
        assertEquals(33, list.get(1),0);
        assertEquals(44, list.get(2),0);
        assertEquals(55, list.get(3),0);
    }

    @Test
    public void get() {
        MyList<Integer> list = new MyArrayList<Integer>();
        list.add(22);
        list.add(33);
        list.add(44);
        list.add(55);
        assertEquals(22, list.get(0),0);
        assertEquals(33, list.get(1),0);
        assertEquals(44, list.get(2),0);
        assertEquals(55, list.get(3),0);
    }

    @Test
    public void delete() {
        MyList<Integer> list = new MyArrayList<Integer>();
        list.add(22); //0
        list.add(33); //1
        list.add(44); //2
        list.add(55);
        list.delete(1);
        assertEquals(3, list.size());
        assertEquals(22, list.get(0),0);
        assertEquals(44, list.get(1),0);
        assertEquals(55, list.get(2),0);
    }
    @Test
    public void deleteString() {
        MyList<String> list = new MyArrayList<String>();
        list.add("22"); //0
        list.add("33"); //1
        list.add("44"); //2
        list.add("55");
        list.delete(1);
        assertEquals(3, list.size());
        assertEquals("22", list.get(0));
        assertEquals("44", list.get(1));
        assertEquals("55", list.get(2));
    }
    @Test
    public void contains() {
        MyList<String> list = new MyArrayList<>();
        list.add("a"); //0
        list.add("b"); //1
        list.add("c"); //2
        assertTrue(list.contains("a"));
        assertFalse(list.contains("aa"));

    }
}