import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MyHasMapTest {

    @Test
    public void size() {
        MyMap<String, String> map = new MyHasMap<>();
        assertEquals(0, map.size());
        map.put("key1", "value1");
        assertEquals(1, map.size());
    }

    @Test
    public void put() {
        MyMap<String, String> map = new MyHasMap<>();
        for (int i = 0; i < 36; i++) {
            map.put("key-"+i, "value-"+i);
        }

        assertEquals(36, map.size());
        for (int i = 0; i < 36; i++) {
            assertEquals("value-"+i, map.get("key-"+i));
        }
    }

    @Test
    public void get() {
    }

    @Test
    public void contains() {
    }
}