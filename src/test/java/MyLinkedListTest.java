import static org.junit.Assert.*;

import org.junit.Test;

public class MyLinkedListTest {

    @Test
    public void size() {
        MyList<String> list = new MyLinkedList<>();
        assertEquals(0, list.size());
        list.add("a");
        assertEquals(1, list.size());
        list.add("b");
        assertEquals(2, list.size());

    }

    @Test
    public void add() {
    }

    @Test
    public void get() {
        MyList<String> list = new MyLinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        assertEquals(3, list.size());
        assertEquals("a", list.get(0));
        assertEquals("b", list.get(1));
        assertEquals("c", list.get(2));

    }

    @Test
    public void delete() {
        MyList<String> list = new MyLinkedList<>();
        list.add("a"); //0
        list.add("b"); //1
        list.add("c"); //2
        assertEquals(3, list.size());
        list.delete(1);
        assertEquals(2, list.size());
        assertEquals("a", list.get(0));
        assertEquals("c", list.get(1));
    }

    @Test
    public void contains() {
        MyList<String> list = new MyLinkedList<>();
        list.add("a"); //0
        list.add("b"); //1
        list.add("c"); //2
        assertTrue(list.contains("a"));
        assertFalse(list.contains("aa"));
    }
}